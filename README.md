# Net Persistence


Graphs and networks are fundamental tools in both data representation and processing. We introduce a general framework in which persistence can be defined directly on objects like weighted graphs and networks, without the need of auxiliary topological constructions.

This code allows to replicate the examples on weighted graphs described in "Beyond topological persistence: Starting from networks", concerning steady and ranging persistence functions. It also includes classes to operate on weighted graphs and compute  persistence diagrams. The functions to plot persistence diagrams have been adapted from [GUDHI](http://gudhi.gforge.inria.fr/).


## Getting Started

### Prerequisites

Albeit not mandatory we recommend to install this package in a virtual environment

### Installing

Clone or download the repository and open a terminal in the folder where the repository has been saved.
Create a virtual environment

```
mkvirtualenv net_persistence_env

```

Install

```
pip install -e ./ 
```

or

```
pip install ./
```
## Example

See [toy_example.py](toy_example.py) for an example of computation of ranging persistence on the Eulerian set-filtration.

![](euler_st_rang.png)

## Authors

* **Mattia G. Bergomi**
* **Massimo Ferri**
* **Pietro Vertechi**
* **Lorenzo Zuffi**


## License

See the [LICENSE.md](LICENSE.md) file.
