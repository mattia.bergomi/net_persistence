from wgraph import WGraph
import platform
if platform.system() == "Darwin":
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt

def main(g1=None, g2=None):
    """Generate the persistence diagrams for the
    stability counterexample of ranging persistence functions
    """
    if g1 is None:
        g1 = [('a', 'b', 1), ('a', 'c', 8), ('a', 'd', 7), ('a', 'f', 6),
              ('b', 'c', 2), ('c', 'd', 3), ('c', 'f', 10), ('d', 'e', 4),
              ('d', 'f', 9), ('e', 'f', 5)]
    if g2 is None:
        g2 = [('a', 'b', 1), ('a', 'c', 8), ('a', 'd', 6), ('a', 'f', 7),
              ('b', 'c', 2), ('c', 'd', 3), ('c', 'f', 10), ('d', 'e', 4),
              ('d', 'f', 9), ('e', 'f', 5)]

    graph = WGraph(g1)
    graph.build_graph()
    graph.build_filtered_subgraphs()
    graph.get_eulerian_filtration()
    graph.ranging_persistence()
    graph.plot_ranging_persistence_diagram(title = "graph1")

    graph_ = WGraph(g2)
    graph_.build_graph()
    graph_.build_filtered_subgraphs()
    graph_.get_eulerian_filtration()
    graph_.ranging_persistence()
    graph_.plot_ranging_persistence_diagram(title = "graph2")
    plt.show()

if __name__ == "__main__":
    main()
