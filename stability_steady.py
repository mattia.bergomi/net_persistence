from wgraph import WGraph
import platform
if platform.system() == "Darwin":
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt

def main(g1=None, g2=None):
    """Generate the persistence diagrams for the
    stability counterexample of steady persistence functions
    """
    if g1 is None:
        g1 = [('a', 'b', 5), ('a', 'c', 11), ('a', 'f', 4), ('a', 'e', 10),
              ('b', 'c', 6), ('b', 'd', 2), ('b', 'f', 1), ('c', 'd', 7),
              ('c', 'e', 12), ('d', 'e', 8), ('d', 'f', 3), ('e', 'f', 9)]
    if g2 is None:
        g2 = [('a', 'b', 5), ('a', 'c', 11), ('a', 'f', 4), ('a', 'e', 9),
              ('b', 'c', 6), ('b', 'd', 2), ('b', 'f', 1), ('c', 'd', 7),
              ('c', 'e', 12), ('d', 'e', 8), ('d', 'f', 3), ('e', 'f', 10)]

        graph = WGraph(g1)
        graph.build_graph()
        graph.build_filtered_subgraphs()
        graph.get_eulerian_filtration(superset =True)
        graph.steady_persistence()
        graph.plot_steady_persistence_diagram(title = "graph1")

        graph_ = WGraph(g2)
        graph_.build_graph()
        graph_.build_filtered_subgraphs()
        graph_.get_eulerian_filtration(superset =True)
        graph_.steady_persistence()
        graph_.plot_steady_persistence_diagram(title = "graph2")





if __name__ == "__main__":
    main()
    plt.show()
