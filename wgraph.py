import numpy as np
import networkx as nx
import platform
if platform.system() == "Darwin":
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
from utils import get_powerset_, get_supersets
from persistence_diagram import CornerPoint, PersistenceDiagram
from itertools import chain

class WGraph:
    """
    Attributes
    ----------

    weighted_edges : list
        list of tuples of the form [(v1, v2, w12), ...] where v1 and v2 are the
        labels associated to the vertices on the boundary of the edge and w12
        is its weight.
    """
    def __init__(self, weighted_edges):
        self.weighted_edges = weighted_edges

    def build_graph(self):
        """Builds the graph defined by the collection of weighted edges
        """
        self.G = nx.Graph()

        for v1, v2, w12 in self.weighted_edges:
            self.G.add_edge(v1, v2, weight = w12)

    def get_edge_filtration_values(self, subgraph = None,
                                    weight_transform = None):
        """Creates list of nodes and edges. Applies filtrating function to the
        weights of the edges and stores the values of the filtrating function
        in an array according to the ordering used to sort the edges in networkx
        edges dictionary
        """
        if subgraph is None:
            subgraph = self.G
        self.nodes = list(subgraph.nodes)
        self.edges = nx.get_edge_attributes(subgraph,'weight').keys()
        self.evaluate_weight_transform_and_set_on_edges(subgraph,
                                                            weight_transform)

    def evaluate_weight_transform_and_set_on_edges(self, subgraph,
                                                        weight_transform):
        """Creates a dictionary edge: value_of_the_weight_transform.

        Notes
        -----
        Does not use nx.set_edge_attributes. To be updated after networkx bug
        correction.
        See link_to_the_reported_issue
        """
        self.transformed_edges = self.get_filtration_values(subgraph,
                                                            weight_transform)
        self.transformed_edges = list(self.transformed_edges.item())
        self.transformed_edges_dict = {edge: value for edge, value in
                                       zip(self.edges, self.transformed_edges)}

    @staticmethod
    def identity(array):
        """Standard filtrating function: do nothing
        """
        return array

    def get_filtration_values(self, subgraph, func):
        """Evaluates func on the weights defined on the edges
        """
        return func(np.asarray(nx.get_edge_attributes(subgraph,'weight').values()))


    def get_subgraph_edges(self, value):
        """Returns the edges of self.G part of the sublevel set defined by value
        """
        return [edge + tuple([self.transformed_edges_dict[edge]])
                    for edge in self.transformed_edges_dict
                    if self.transformed_edges_dict[edge] <= value]

    @staticmethod
    def get_subgraph(edges):
        """Returns the subgraph defined by edges. Once the filtration is
        generated we are only interested in the 'hubbiness' of the nodes in the
        subgraph. Thus we do not set weights.
        """
        H = nx.Graph()
        [H.add_edge(v1, v2, weight =  np.round(w12, decimals = 2))
         for v1, v2, w12 in edges]
        return H

    def build_filtered_subgraphs(self, weight_transform = None):
        """Generates the filtration of G given the values of the filtrating
        function.
        """
        if weight_transform is None:
            weight_transform = self.identity

        self.weight_transform = weight_transform
        self.get_edge_filtration_values(weight_transform = weight_transform)
        self.filtration = []
        self.transformed_edges = np.unique(self.transformed_edges)
        self.transformed_edges.sort()

        for value in self.transformed_edges:
            edges = self.get_subgraph_edges(value)
            self.filtration.append(self.get_subgraph(edges))

    def get_eulerian_filtration(self, superset = False):
        if superset:
            self.eulerians = {sublevel:
                              get_supersets(self.get_eulerian_subgraphs(subgraph))
                              for sublevel, subgraph in enumerate(self.filtration)}
        else:
            self.eulerians = {sublevel:
                              self.get_eulerian_subgraphs(subgraph)
                              for sublevel, subgraph in enumerate(self.filtration)}
        cornerpoint_vertices_ = list(chain.from_iterable(self.eulerians.values()))
        self.cornerpoint_vertices = []

        for s in cornerpoint_vertices_:
            if s not in self.cornerpoint_vertices:
                self.cornerpoint_vertices.append(s)

        self.persistence = {i: [k for k in self.eulerians
                            if cornerpoint_vertex in self.eulerians[k]]
                            for i, cornerpoint_vertex in enumerate(self.cornerpoint_vertices)}
        self.persistence = {key: value for key, value
                                in self.persistence.items()
                                if len(value) >= 1}

    @staticmethod
    def get_maximum_steady_persistence(array):
        """Return list of consecutive lists of numbers from vals (number list)."""
        sub_persistence = []
        sub_persistences = [sub_persistence]
        consecutive = None

        for value in array:
            if (value == consecutive) or (consecutive is None):
                sub_persistence.append(value)
            else:
                sub_persistence = [value]
                sub_persistences.append(sub_persistence)
            consecutive = value + 1

        sub_persistences.sort(key=len)

        return sub_persistences

    def steady_persistence(self):
        """Ranks steady hubs according to their persistence. Recall that a
        temporary hub is steady if it lives through consecutive sublevel sets of
        the filtration induced by the weights of the graph.
        """
        self.steady_cornerpoints = []

        for vertices in self.persistence:
            pers = self.persistence[vertices]
            # if len(pers) > 1:
            max_steady_pers = self.get_maximum_steady_persistence(pers)
            births = [min(c) for c in max_steady_pers]
            deaths = [max(c) for c in max_steady_pers]
            deaths = [np.inf if d == len(self.filtration)-1
                      else self.transformed_edges[d + 1] for d in deaths]
            # death = np.inf if death == len(self.filtration)-1 else self.transformed_edges[death + 1]
            for birth,death in zip(births, deaths):
                c = CornerPoint(0,
                                self.transformed_edges[birth],
                                death,
                                vertices = self.cornerpoint_vertices[vertices])
                self.steady_cornerpoints.append(c)

        self.steady_pd = PersistenceDiagram(cornerpoints = self.steady_cornerpoints)

    def ranging_persistence(self):
        """Ranks ranging hubs according to their persistence. Recall that a
        temporary hub is said ranging if there exist Gm and Gn sublevel sets
        (m < n) in which the temporary hub is alive.
        """
        self.ranging_cornerpoints = []

        for vertices in self.persistence:
            pers = self.persistence[vertices]
            # if len(pers) >= 1:
            birth = min(pers)
            death = max(pers)
            death = np.inf if death == len(self.filtration)-1 else self.transformed_edges[death + 1]
            c = CornerPoint(0,
                            self.transformed_edges[birth],
                            death,
                            vertices = self.cornerpoint_vertices[vertices])
            self.ranging_cornerpoints.append(c)

        self.ranging_pd = PersistenceDiagram(cornerpoints = self.ranging_cornerpoints)

    @staticmethod
    def get_eulerian_subgraphs(graph):
        return [set(comb) for comb in get_powerset_(list(graph.nodes))
                if nx.is_eulerian(graph.subgraph(comb))]

    def plot_filtration(self):
        """Plots all the subgraphs of self.G given by considering the sublevel
        sets of the function defined on the weighted edges
        """
        fig, self.ax_arr = plt.subplots(int(np.ceil(len(self.filtration) / 3)),3)
        self.ax_arr = self.ax_arr.ravel()
        ordinals = ['st', 'nd', 'rd']
        for i, h in enumerate(self.filtration):
            title = str(i + 1) + ordinals[i] if i < 3 else str(i + 1) + 'th'
            self._draw(graph = h, plot_weights = True, ax = self.ax_arr[i],
                        title = title + " sublevel set")


    def _draw(self, graph = None, plot_weights = True, ax = None, title = None):
        """Plots a graph using networkx wrappers

        Parameters
        ----------
        graph : <networkx graph>
            A graph instance of networkx.Graph()
        plot_weights : bool
            If True weights are plotted on the edges of the graph
        ax : <matplotlib.axis._subplot>
            matplotlib axes handle
        title : string
            title to be attributed to ax
        """
        if graph is None:
            graph = self.G
        pos = nx.shell_layout(graph)
        if title is not None:
            ax.set_title(title)
        nx.draw(graph, pos = pos, ax = ax, with_labels = True)
        if plot_weights:
            labels = nx.get_edge_attributes(graph,'weight')
            labels = {k : np.round(v, decimals = 2) for k,v in labels.items()}
            nx.draw_networkx_edge_labels(graph, pos, edge_labels = labels,
                                        ax = ax)

    def plot_steady_persistence_diagram(self, title = None):
        """Uses gudhi and networkx wrappers to plot the persistence diagram and
        the hubs obtained through the steady-hubs analysis, respectively
        """
        fig, ax = plt.subplots()
        self.steady_pd.plot_gudhi(ax,
                persistence_to_plot = self.steady_pd.persistence_to_plot)
        if title is not None:
            plt.suptitle(title)

    def plot_ranging_persistence_diagram(self, title = None):
        """Uses gudhi and networkx wrappers to plot the persistence diagram and
        the hubs obtained through the ranging-hubs analysis, respectively
        """
        fig, ax = plt.subplots()
        self.ranging_pd.plot_gudhi(ax,
                persistence_to_plot = self.ranging_pd.persistence_to_plot)
        if title is not None:
            plt.suptitle(title)


if __name__ == "__main__":
    example = [('a', 'b', 14),
               ('b', 'c',  4),
               ('c', 'd',  6),
               ('d', 'e',  5),
               ('e', 'f', 12),
               ('f', 'g',  8),
               ('g', 'h',  9),
               ('i', 'f', 11),
               ('i', 'a', 13),
               ('i', 'b',  2),
               ('i', 'c',  3),
               ('c', 'f', 15),
               ('c', 'e',  7),
               ('h', 'f', 10)]

    graph = WGraph(example)
    graph.build_graph()
    graph.build_filtered_subgraphs()
    graph.get_eulerian_filtration()
    graph.steady_persistence()
    graph.plot_steady_persistence_diagram()
